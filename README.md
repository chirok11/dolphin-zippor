# dolphin-zipper

Usage:
```
const zipper = require('dolphin-zipper');
const arc = require('dolphin-zipper');

arc.archivate(output_zip_path, path_to_directory, [file_list]);
```

if you wan't to use glob's please use then like this
  file_list example:

  ['Default/Cookies',
  'Default/Preferences',
  'Default/Extensions/\*\*/\*',
  'Default/Extension State/\*\*/\*']
