use glob::glob;
use std::{
    ffi::OsStr,
    fs::{metadata, File},
    io::{Read, Write},
    path::Path,
};
use zip::{write::FileOptions, ZipWriter};

pub fn archivate_it<'a>(
    file_path: &'a str,
    datadir_path: &'a str,
    filelist: Vec<String>,
) -> zip::result::ZipResult<()> {
    let path = Path::new(file_path);
    let ddir = Path::new(datadir_path);
    let file = File::create(&path).unwrap();

    let mut zip = ZipWriter::new(file);
    // zip.add_directory("Default", Default::default())?;

    let options = FileOptions::default()
        .compression_method(zip::CompressionMethod::Deflated)
        .unix_permissions(0o755);

    for file in filelist {
        match file.contains("*") {
            true => {
                // this is glob.
                // go to walkdir
                for entry in glob(&file).expect("unable to glob") {
                    match entry {
                        Ok(path) => {
                            if path.is_file() {
                                // zip.add_directory_from_path(&path.as_path(), Default::default())?;
                                zip.start_file(path.to_str().unwrap(), options)?;
                                let data = get_bytes_by_filename(ddir.join(&path).as_os_str());
                                zip.write_all(&data)?;
                            }
                        }
                        Err(e) => {
                            eprintln!("{:?}", e)
                        }
                    }
                }
            }
            false => {
                zip.start_file(&file, options)?;
                let data = get_bytes_by_filename(ddir.join(file).as_os_str());
                zip.write_all(&data)?;
            }
        }
    }

    zip.finish()?;

    Ok(())
}

fn get_bytes_by_filename(filename: &OsStr) -> Vec<u8> {
    let f = File::open(&filename);
    match f {
        Ok(mut f) => {
            let fmeta = metadata(&filename).expect("unable to read metadata");
            let mut buffer = vec![0; fmeta.len() as usize];
            f.read(&mut buffer).expect("buffer overflow");

            buffer
        }
        Err(_) => {
            return Vec::new();
        }
    }
}

#[test]
fn test_archivate_it() {
    println!("place Default folder in root");
    println!("and we'll try to archivate it");

    let file_list = vec![
        "Default/Bookmarks".into(),
        "Default/Cookies".into(),
        "Default/Favicons".into(),
        "Default/History".into(),
        "Default/Preferences".into(),
        "Default/User Preferences".into(),
        "Default/Secure Preferences".into(),
        "Default/Extension State/**/*".into(),
        // add glob variants
        "Default/Local Storage/**/*".into(),
        "Default/Extensions/**/*".into(),
        "Default/Extension Rules/**/*".into(),
        "Default/Extension State/**/*".into(),
        // why ?
        "Default/IndexedDB/**/*".into(),
    ];

    archivate_it("admin.zip", "", file_list).unwrap();
}
