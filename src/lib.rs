#![deny(clippy::all)]

#[macro_use]
extern crate napi_derive;
mod zip;

use napi::{CallContext, JsNumber, JsObject, JsString, JsTypedArray, Result};

#[module_exports]
fn init(mut exports: JsObject) -> Result<()> {
    exports.create_named_method("archivate", archivate)?;
    Ok(())
}

#[js_function(2)]
fn archivate(ctx: CallContext) -> Result<JsNumber> {
    let zzip_path = ctx.get::<JsString>(0)?.into_utf8()?;
    let ddir_path = ctx.get::<JsString>(1)?.into_utf8()?;
    let mut file_list = Vec::new();
    let files_a = ctx.get::<JsTypedArray>(2)?;
    let len = files_a.get_array_length()?;
    for l in 0..len {
        let name = files_a
            .get_element::<JsString>(l)?
            .into_utf8()?
            .as_str()?
            .to_string();
        file_list.push(name);
    }

    let r = zip::archivate_it(zzip_path.as_str()?, ddir_path.as_str()?, file_list);
    match r {
        Ok(_) => {
            return ctx.env.create_int32(0);
        }
        Err(_) => {
            return ctx.env.create_int32(9);
        }
    }
}
